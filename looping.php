<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Looping</title>
</head>
<body>
    <h1>Berlatih Looping PHP</h1>
    <?php
    echo "<h3>Soal No 1</h3>";
    echo "<h5>LOOPING PERTAMA</h5>";
    for($i=2; $i<=20; $i+=2){
        echo $i . " - I LOVE PHP <br>";
    }
    echo "<h5>LOOPING KEDUA</h5>";
    for($a=20; $a>=2; $a-=2){
        echo $a . " - I LOVE PHP <br>";
    }

    echo "<h3>Soal No 2</h3>";
    $numbers = [18, 45, 29, 61, 47, 34];
    echo "Array numbers : ";
    print_r($numbers);
    echo "<br>";
    echo "Array sisa baginya adalah";
    echo "<br>";
    foreach($numbers as $value){
        $rest[] = $value %= 5; 
    }
    print_r($rest);
    echo "<br>";

    echo "<h3>Soal No 3</h3>";
    $data = [
        ["001", "Keyboard Logitek", "60000", "Keyboard yang mantap untuk kantoran", "logitek.jpeg"],
        ["002", "Keyboard MSI", "300000", "Keyboard gaming MSI mekanik", "msi.jpeg"],
        ["003", "Mouse Genius", "50000", "Mouse Genius biar lebih pinter", "genius.jpeg"],
        ["004", "Mouse Jerry", "30000", "Mouse yang disukai kucing", "jerry.jpeg"],
    ];
    foreach($data as $key => $value){
        $item = array(
            'id' => $value[0],
            'name' => $value[1],
            'price' => $value[2],
            'description' => $value[3],
            'source' => $value[4],
        );
        print_r($item);
        echo "<br>";
    }
    echo "<h3>Soal No 4</h3>";
    for($j=1; $j<=5; $j++){
        for($b=1; $b<=$j; $b++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>